#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n, q, m;
    cin >> n >> q;

    int* a[n];
    for (int i = 0; i < n; i++) {
        cin >> m;
        a[i] = new int[m];
        for (int j = 0; j < m; j++) {
            cin >> a[i][j];

        }

    }
    for (int i = 0; i < q; i++) {
        int b, c;
        cin >> b >> c;
        cout << a[b][c] << "\n";
    }

    return 0;
}