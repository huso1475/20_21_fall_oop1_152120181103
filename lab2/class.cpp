#include <iostream>
#include <sstream>
using namespace std;

class Student {
    int age;
    string first_name;
    string last_name;
    int standard;
public:
    void set_age(int xage) {
        age = xage;
    }
    void set_first_name(string xfirstname) {
        first_name = xfirstname;
    }
    void set_last_name(string xlastname) {
        last_name = xlastname;
    }
    void set_standard(unsigned xstandard) {
        standard = xstandard;
    }
    int get_age() {
        return age;
    }
    string get_first_name() {
        return first_name;
    }
    string get_last_name() {
        return last_name;
    }
    int get_standard() {
        return standard;
    }
    string to_string() {
        string text = ::to_string(age) + ',' + get_first_name() + ',' + get_last_name() + ',' + ::to_string(standard);
        return text;
    }
};


int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();

    return 0;
}