#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int size;
    int a;
    cin >> size;
    vector<int>v;
    for (int i = 0; i < size; i++) {
        cin >> a;
        v.push_back(a);
    }
    cin >> a;
    v.erase(v.begin() + a - 1);
    int b, c;
    cin >> b >> c;
    v.erase(v.begin() + b - 1, v.begin() + c - 1);
    cout << v.size() << endl;
    for (auto _v : v)
        cout << _v << " ";
    cout << endl;
    return 0;
}
