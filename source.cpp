#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <math.h>
#include <list>
#include<ctype.h>
using namespace std;
int findsum(int veriler[100], int n) {
	int sum = 0;
	for (int i = 0; i < n; i++) {
		
		sum = sum + veriler[i];
		
	}
	return sum;
}
int findproduct(int veriler[], int n) {
	int product = 1;
	for (int i = 0; i < n; i++) {

		product = product * veriler[i];

	}
	return product;
}
int findaverage(int sum, int n) {
	float average = (float)sum / n;
	return average;
}
int findsmallest(int veriler[], int n) {
	int smallest = veriler[0];
	for (int i = 0; i < n; i++) {
		if (smallest > veriler[i])
			smallest = veriler[i];
	}
	return smallest;
}
void main() {
	int smallest;
	int sayi;

	int n;
	int veriler[100];
	ifstream dataFile;
	dataFile.open("input.txt", fstream::out);//file opened here
	if (!dataFile)
	{
		cout << "File not found!\n";
		exit(0);
	}
	else {
		
		dataFile >> n;//read first row 

		for (int i = 0; i < n; i++) {
			dataFile >> sayi;

			veriler[i] = sayi;

		}//initilizing values to array

		cout << "Sum is " << findsum(veriler, n) << "\n";
		cout << "Product is " << findproduct(veriler, n) << "\n";
		cout << "Average is " << findaverage(findsum(veriler, n), n) << "\n";
		cout << "Smallest is " << findsmallest(veriler, n) << "\n";
	}
	system("pause");
	

	
}